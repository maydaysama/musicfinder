import requests
import re
import json
from random import seed, randint
from bs4 import BeautifulSoup

class Scrap():
    headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", 
		"Accept-Encoding": "gzip, deflate, br", 
		"Accept-Language": "fr-FR,fr;q=0.9,en-US;q=0.8,en;q=0.7,uz;q=0.6", 
		"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.122 Safari/537.36"
    }

    def __init__(self, inst, url):
        self.inst = inst
        self.url = url
        self.brand = []
        self.model = []
        self.image = []
        self.price = []
        self.stock = []

    def run(self):
        seed(1)
        page = requests.get(self.url, timeout=30, headers=self.headers)
        soup = BeautifulSoup(page.content, 'html.parser')
        soup.prettify()
        items = soup.find_all(class_="extensible-article list-view compare parent")
        for item in items:  
            titleBlock = item.find(class_="title-block link-group")
            self.brand.append(titleBlock.find(class_="manufacturer").get_text())
            self.model.append(titleBlock.find(class_="model").get_text())
            self.image.append(item.find(class_="left").img["src"])
            self.price.append(re.sub("([.€])+", "", item.find(class_="article-basketlink").get_text())[:-1])
            self.stock.append(randint(0, 30))

    def toJSON(self):
        jsonFile = ""
        for i in range(len(self.brand)):
            jsonFile += json.dumps({
            "inst": self.inst,
            "brand": self.brand[i],
            "model": self.model[i],
            "image": self.image[i],
            "price": self.price[i],
            "stock": self.stock[i]
            })
            if i < len(self.brand) - 1:
                jsonFile += ","
        return jsonFile

    def toPostgreSQL(self):
        queries = ""
        for i in range(len(self.brand)):
            queries += f"INSERT INTO \"instruments\" (\"inst\", \"brand\", \"model\", \"price\", \"stock\", \"image\") VALUES('{self.inst}', '{self.brand[i]}', '{self.model[i]}', '{self.price[i]}', '{self.stock[i]}', '{self.image[i]}');\n"
        return queries

    @staticmethod
    def writeJSON(*args):
        file = open("dump.json", "w")
        content = "["
        for arg in args:
            content += f"{arg.toJSON()},"
        content = content[0:-1] + "]"
        file.write(content)

    @staticmethod
    def writePostgreSQL(*args):
        file = open("init.sql", "w")
        file.write('CREATE TABLE IF NOT EXISTS "instruments" ("id" serial PRIMARY KEY, "inst" VARCHAR (64) NOT NULL, "brand" VARCHAR (64) NOT NULL, "model" VARCHAR (128) NOT NULL, "price" INT, "stock" INT, "image" VARCHAR (256) NOT NULL);\n')
        content = ""
        for arg in args:
            content += arg.toPostgreSQL()
        file.write(content)

guitar = Scrap("guitar", "https://www.thomann.de/fr/st_modelle.html?ls=100")
keyboard = Scrap("keyboard", "https://www.thomann.de/fr/workstations.html")
bass = Scrap("bass", "https://www.thomann.de/fr/basses_j_4_cordes.html?ls=100")
turntables = Scrap("turntables", "https://www.thomann.de/fr/turntables.html?ls=100")
drums = Scrap("drums", "https://www.thomann.de/fr/sets_batterie_complets.html?ls=100")

guitar.run()
keyboard.run()
bass.run()
turntables.run()
drums.run()

Scrap.writePostgreSQL(guitar, keyboard, bass, turntables, drums)
Scrap.writeJSON(guitar, keyboard, bass, turntables, drums)