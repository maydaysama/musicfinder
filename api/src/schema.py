from graphene import ObjectType, Field, Int, String, List, Schema
from graphene_sqlalchemy import SQLAlchemyObjectType
from sqlalchemy import or_, and_
from models import Instruments as InstrumentsModel
from db import db_session

class Instruments(SQLAlchemyObjectType):
    class Meta:
        model = InstrumentsModel

class Query(ObjectType):
    instruments = Field(Instruments, id=Int())
    allInstruments = List(Instruments, inst=String(required=False))

    def resolve_instruments(self, info, id):
        user = db_session.query(InstrumentsModel).filter_by(id=id).first()
        return user

    def resolve_allInstruments(self, info, **kwargs):
        if "inst" in kwargs:
            users = db_session.query(InstrumentsModel).filter_by(inst=kwargs["inst"]).all()
        else:
            users = db_session.query(InstrumentsModel).all()
        return users

schema = Schema(query=Query)