from sqlalchemy import Table, Column, Integer, String, Sequence
from sqlalchemy.ext.declarative import declarative_base
from db import db_session

Base = declarative_base()
Base.query = db_session.query_property()

class Instruments(Base):  
    __tablename__ = 'instruments'

    id = Column(Integer, Sequence('instruments_id_seq'), primary_key=True)
    inst = Column(String, nullable=False)
    brand = Column(String, nullable=False)
    model = Column(String, nullable=False)
    price = Column(Integer)
    stock = Column(Integer)
    image = Column(String, nullable=False)

    def __repr__(self):
        return f'<User(id={self.id}, inst={self.inst}, brand={self.brand}, model={self.model}, price={self.price}, stock={self.stock}, image={self.image})>'
    
    def __json__(self):
        return { "inst": self.inst, "brand": self.brand, "model": self.model, "price": self.price, "stock": self.stock, "image": self.image }
    
    #__mapper_args__ = {
    #    'polymorphic_on' : type
    #}