from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, scoped_session

db_string = "postgres://postgres:123456@postgres:5432"
db = create_engine(db_string)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=db)) 