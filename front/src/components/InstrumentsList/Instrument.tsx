import React from 'react';
import { Avatar, makeStyles, Card, CardHeader } from '@material-ui/core';
import { I_Instrument } from '../../types';

const useStyles = makeStyles(theme => ({
    avatar: {
        width: theme.spacing(6),
        height: theme.spacing(6)
    },
    article: {
        marginTop: "10px",
        marginBottom: "10px",
        maxWidth: "300px",
        minWidth: "300px",
    },
}));

const Instrument: React.FC<I_Instrument> = ({ brand, model, image, price }) => {
    const classes = useStyles();
    return (
        <Card className={classes.article}>
            <CardHeader
                avatar={
                    <Avatar src={image} className={classes.avatar} />
                }
                action={`${price} €`}
                title={brand}
                subheader={model}
            />
        </Card>
    );
};

export default Instrument;