import React from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import Instrument from './Instrument';
import { I_Instrument } from '../../types';
import { Grid } from '@material-ui/core';

const ALL_INSTRUMENTS = gql`
    {
        allInstruments (inst: "guitar") {
            id
            brand
            model
            price
            image
        }
    }
`;


const InstrumentsList = (props: any) => {
    const { loading, error, data } = useQuery(ALL_INSTRUMENTS);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>console.log(error)</p>

    return (
        <div>
            <Grid container spacing={1}>
                    {
                        data.allInstruments.map((inst: I_Instrument) => (
                            <Grid item xs={4}>
                                <Instrument
                                    key={inst.id}
                                    id={inst.id}
                                    brand={inst.brand}
                                    model={inst.model}
                                    price={inst.price}
                                    image={inst.image}
                                    stock={inst.stock}
                                />
                            </Grid>
                        ))
                    }
            </Grid>
        </div>
    );
};

export default InstrumentsList;