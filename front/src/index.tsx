import React from 'react';
import ReactDOM from 'react-dom';
import { ApolloProvider } from '@apollo/react-hooks';
import { client } from './apollo';
import { InstrumentsList } from './components';

const App = () => {
    return (
        <ApolloProvider client={client}>
            <InstrumentsList />
        </ApolloProvider>
        
    );
};

ReactDOM.render(<App />, document.getElementById("root"));