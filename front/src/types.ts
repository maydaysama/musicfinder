export interface I_Instrument {
    id: number,
    model: string,
    brand: string,
    stock: number,
    price: number,
    image: string
}